import React, { useState } from "react";
import { connect } from "react-redux";
import {
  increment,
  decrement,
  fivePlus,
  isLoggedAction,
  AddName,
} from "../actions";

import { useSelector, useDispatch } from "react-redux";

function Counter(props) {
  const counter = useSelector((state) => state.counter);
  const isLogged = useSelector((state) => state.isLogged);
  const names = useSelector((state) => state.names);

  const listName = names.names;
  console.log(listName);

  const dispatch = useDispatch();

  const SubmitName = (value) => {
    dispatch(AddName(value));
  };

  const [valuename, setValueName] = useState();

  return (
    <div>
      <h1> Your Points : {counter}</h1>

      <button onClick={() => dispatch(increment())}> PLUS </button>
      <button onClick={() => dispatch(decrement())}> MINUS </button>
      <button onClick={() => dispatch(fivePlus(5))}> + FIVE </button>

      <h2> {isLogged ? "User is logged in " : "User is not loggedin "}</h2>
      <button onClick={() => dispatch(isLoggedAction())}>Log in now</button>
      {names && (
        <div>
          <h1> All Names</h1>
          {listName.map((name) => {
            return <h3>{name}</h3>;
          })}
        </div>
      )}

      <div>
        <br />
        <form>
          <label>
            Name:
            <input
              type='text'
              name='name'
              onChange={(e) => setValueName(e.target.value)}
            />
          </label>
          <input
            type='submit'
            value='Submit'
            onSubmit={SubmitName(valuename)}
          />
        </form>
      </div>
    </div>
  );
}

export default Counter;
