import { combineReducers } from "redux";
import counterReducer from "./counterReducer";
import isLoggedReducer from "./isLoggedReducer";
import nameReducer from "./NameReducer";

const allReducers = combineReducers({
  counter: counterReducer,
  isLogged: isLoggedReducer,
  names: nameReducer,
});

export default allReducers;
