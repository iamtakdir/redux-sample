import { ADD_NAME, NAME } from "../actions/NameActionType";

let initialNameState = {
  names: [],
};

const nameReducer = (state = initialNameState, action) => {
  switch (action.type) {
    case NAME:
      return {
        state,
      };

    case ADD_NAME:
      return { names: [...state.names, action.payload] };

    default:
      return state;
  }
};

export default nameReducer;
