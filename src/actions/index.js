//Importing Types
import {
  INCREMENT, DECREMENT,FIVE_PLUS
} from "./CounterActionType"
//
import { SIGN_IN } from "./LoggedInActionType";
// 
import {NAME, ADD_NAME} from "./NameActionType"


//Action Creator
export const increment = () => {
  //return the action
  // action has 2 thing 1. Type 2. Payload

  return {
    type: INCREMENT,
  };
};

export const decrement = () => {
  return {
    type: DECREMENT,
  };
};

export const fivePlus = (num) => {
  return {
    type: FIVE_PLUS,
    payload: num,
  };
};

export const isLoggedAction = () => {
  return {
    type: SIGN_IN,
  };
};

//Name ACTION

export const Names = () => {
  return {
    type:NAME
  }
}

export const AddName = (val) => { 
  return {
    type: ADD_NAME,
    payload: val
  }
}